<?php 


class ViewAdherent{
	private $centre;
	 private $adherent;
   private $category;
   private $type;
   private $document;
   private $titre_doc;
   private $loan;
   public function __construct() {
    

  }


  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }
   

    public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }

 

public function Login_form(){
      $html="<div class='content_cnx'><div id='cnx' >
            <div class='cnx_title'><h4>Connexion</h4></div>   
            <form  action='login.php' method='post'>
            <input id='user' placeholder='Username' type='text' name='username' /><br>
            <input id='pwd' placeholder='Password' type='password' name='pwd' /><br>
            <input class='btn_login' type='submit' value='Login' name='login'/>
          </form>
          </div>
        </div>";

    return $html;
    }


    public function loan_adherent(){
  
  $this->loan=$_SESSION['loan'];
  $html="<div class='result1'>
         
        <div class='d1'>
          <div class='return_search '> <span><a href ='indexAdherent.php'>< Retour à la recherche</a></span></div>  
        </div>        
      </div>";
$html.="<div class='add_emprunt'>";
if(count($this->loan)>0){
 $html.=" <div class='recap col-m-12 col-m-center col-l-10 col-l-center col-s-12'> 
              <div class='recap_title'>
                <h4>Récapitulatif</h4>
              </div>

              <div class='recap_details'>
                <div class='nb'>
                  <strong>Nombre de document :</strong> <span>".count($this->loan)."</span>
                </div>
              </div>

              <div class='tab'>
                <table>
                  <tr>
                    <th>Réference</th>
                    <th>Titre</th>
                    <th>Type</th>
                    <th>Genre</th>
                    <th>date retour</th>
                  </tr>";
                  foreach ($this->loan as $l) {
                     $html.="               
                  <tr>
                    <td>".$l->ref."</td>
                    <td>".$l->title."</td>
                    <td>".$l->name."</td>
                    <td>".$l->category."</td>
                    <td>".$l->return_date."</td>
                  </tr>";}
                  $html.="</table>  
              </div>

              <div class='print'>
              <input type='hidden' name='action' value='loan_adherent'/>
                <input type='submit' value='Imprimer' />
              </div>

            </div>
          </div>";
          }else{
               $html.="<h1>Aucun Emprunt</h1>";                     
            }
  return $html;
}

  public function search_form(){
   $html= "<form method='post' action='indexAdherent.php'>
        <div class='search1'>
        <div class='d1 col-l-4 col-m-5'>
        <div class='label col-m-4 col-s-4'>Mot clé :&nbsp;&nbsp;</div>   
        <input class='loop col-m-2 col-s-2' type='submit' value='' name='search'/>
        <input class='col-m-5 col-s-5' type='text' name='motclé' placeholder='Rechercher'/></div>
        <input type='hidden' name='action' value='c_search'/>
       <div class='d2 col-l-5 col-m-6'>
         
          <div class='label' id='rech_titre_desc'>Rechercher par :</div>
          <div class='radio_word'>
            <input type='radio' name='word' value='titre'><span>Titre</span>
            <input type='radio' name='word' value='description'><span>Descriptif</span>
          </div>
        </div>
        </div>
        <div class='search2 col-l-8 col-l-center col-m-11 col-m-center col-s-12 col-s-center'>
          <fieldset class='col-s-11 col-s-center'>
            <legend>Filtrer par:</legend>
              <div class='search_type'>
                  <div class='label'>Type :</div>
                  <div class='inputs'>";
                   foreach ($this->type as $t) {
                     $html.= " <input type='radio' name='type' value=".$t->id."><span>".$t->name."</span></br>";
                   }
                  $html.="
                  </div>
                </div>

                <div class='search_category'>
                <div class='label'>Genre :</div>
                  <div class='inputs'><select name='category'><option>Choisir Catégorie</option>";
                  foreach ($this->category as $c) {
                     $html.= " <option  value='".$c->id."'>".$c->name."</option>";
                   }
                   $html.="
                    </select>
                  </div>
                </div>
          </fieldset>
        </div>
      </form>";
    return $html;
  }
    

    public function search_result(){
     
      $this->document=$_SESSION['document'];
      
      $result = count($this->document);
    $html="
      <div class='result1'>
         
        <div class='d1'>
          <div class='return_search'> <span><a href ='indexAdherent.php'>< Retour à la recherche</a></span></div>  
        </div>        
      </div>
      <div class='result2 col-m-12 col-m-center col-s-12'>
        <div class='nb_result'> <strong>".$result." Résultat(s) trouvé pour ".$this->titre_doc."</strong> </div>
          ";  
          
         
          if($result>0){
         
           foreach ($this->document as $d) {
                   $html.=" <div class='result_elements'><div class='doc_type'>
              <img  src='stylesheets/img/".$d->id_type.".png' width='50' />
                  </div> 
                <div class='doc_info'>
              <p class='_p'><a href='indexAdherent.php?action=c_search_details&ref="
              .$d->ref."'>".$d->title." </a></p>
              <p>Edition : ".$d->edition."</p></div></div>";
                   
              }
         }
         else{
          $html.="AucunRésultat";
         }
        $html.=" </div></div>"; 

      return $html;
  }
  public function search_details(){
    
    $this->document=$_SESSION['document'];

    $html=" <div class='result1'>
                <div class='return_search'> 
                    <span><a href ='indexAdherent.php'>< Retour à la recherche</a></span>
                </div>
            </div>
            <div class='content_details col-l-10 col-l-center col-m-12 col-m-center'>";
            
              
                   $html.="
              <div class='img_doc' ><img src='stylesheets/img/doc/".$this->document->image."'/></div>
              <div class='info_details'>
              <div class='titre'><strong>Titre : </strong><span>".$this->document->title."</span></div>";
              
              if($this->document->id_state==2)
              $html.="<div class='titre'><strong>Date retour : </strong><span>".$this->document->return_date."</span></div>";
              
              $html.="<img id='img_doc' src='stylesheets/img/".$this->document->id_type.".png' width='50' />
              <div class='edition'><strong>Edition : </strong><span>".$this->document->edition."</span></div>
              <p class='description _p'>".$this->document->description."
              </p> 

              </div>
            </div>";
            
      return $html;

  }

  public function afficheGeneral($sel){

        switch ($sel){

          case 'accueil':{
            $this->centre=$this->search_form();
            break;
          }
          case 'search':{
            $this->centre=$this->search_result();
            break;
          }

          case 'search_details':{
            $this->centre=$this->search_details();
            break;
          }

          case 'Login_form':{
            $this->centre=$this->Login_form();
            break;
          }

          case 'loan_adherent':{
            $this->centre=$this->loan_adherent();
            break;
          }

        }

if(!isset($_SESSION['username'])){ global $action; $action="Login_form";}  
else{global $action; $action="loan_adherent"; global $src; $src="<img width='30' src='stylesheets/img/logout.png'>";} 

echo "
<!DOCTYPE html>
<html lang=\"fr\">
    <head>
        <meta  charset=\"utf-8\"/>
        <title>Ma-Bibliothèque</title>        
        
            <link rel='stylesheet' href='stylesheets/reset.css'/>
        <link rel='stylesheet' href='stylesheets/framework.css'/>
        <link rel='stylesheet' href='stylesheets/custom.css'/>
        <link rel='stylesheet' href='stylesheets/adherent.css'/>
         <link rel='stylesheet' href='stylesheets/grille.css'/>
         <link rel='stylesheet' href='stylesheets/mediumscreen.css'/>
         <link rel='stylesheet' href='stylesheets/smallscreen.css'/>
        
    </head>
<body>\n";
echo"
<div id='content' class='col-l-10 col-l-center col-m-10 col-m-center col-s-12' >\n";


echo"
<header>

  <div class='header'>  
    <div class='title_bib'>  
      <h2>MaBibliothèque</h2> <img src='stylesheets/img/logo.png' width='100' />       
    </div>
    <div class='barre_ad'>
    <a href='indexAdherent.php?action=".$action."'>";
    
    echo"<h5>Mes emprunts</h5></a>";
    if (isset($src)){ echo "<a href='indexAdherent.php?action=logout'>".$src."</a>";}
    echo "</div>
  </div>
</header>\n";

echo "<nav>";
echo "</nav>";


echo"<article>";

echo"</article>";

echo"<aside>";

echo"</aside>";

echo "<section class='home_search'>";
echo $this->centre;
echo "</section>";


echo"<footer class='col-m-10 col-m-10 col-m-center col-l-12 col-l-center col-s-12 col-s-center'><div class='footer'> <h2>Copyright 2014</h2></div></footer>";
echo"
</div>";
echo "</body>";
echo "</html>";
}

}