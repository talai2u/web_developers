<?php 


class ViewStaff{
	private $centre;
	 private $adherent;
   private $category;
   private $type;
   private $document;
   private $loan;
   private $loan_rendu;
   private $loan_restant;
   public function __construct() {
    

  }


  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }
   

    public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }

public function gestion_ad(){
   $html="<div class='home_menu'>
            <div class='menu_staff gestion_ad'><a href='indexStaff.php?action=insert_adherent'><h4>Ajouter Adhérent</h4></a></div>
            <div class='menu_staff gestion_ad'><a href='indexStaff.php?action=update_adherent'><h4>Modifier Adhérent</h4></a></div>
            <div class='menu_staff gestion_ad'><a href='indexStaff.php?action=delete_adherent'><h4>Supprimer Adhérent</h4></a></div>
          </div>";

    return $html;
  }
  
  public function gestion_doc(){
   $html="<div class='home_menu'>
            <div class='menu_staff gestion_ad'><a href='indexStaff.php?action=insert_doc'><h4>Ajouter Document</h4></a></div>
            <div class='menu_staff gestion_ad'><a href='indexStaff.php?action=delete_doc'><h4>Supprimer Document</h4></a></div>
            <div class='menu_staff gestion_ad'><a href='indexStaff.php?action=insert_category'><h4>Ajouter Genre</h4></a></div>
            <div class='menu_staff gestion_ad'><a href='indexStaff.php?action=insert_type'><h4>Ajouter Type</h4></a></div>
          </div>";

    return $html;
  }
  public function home_staff(){
   $html="<div class='home_menu'>
            <div class='menu_emprunt'>
              <strong><a href='indexStaff.php?action=emprunt_staff'>Emprunt des préts </a></strong>
            </div>
            <div class='menu_return'>
              <strong><a href='indexStaff.php?action=return_staff'>Retour des préts</a></strong>
            </div>
          </div>";

    return $html;
  }

  
 public function emprunt_staff(){
    session_start();
    $html="<div class='content_emprunt'>
             <div class='add_emprunt'>
                <fieldset>
                  <legend>Filtrer par:</legend>
                  <form method='post' action=''>
                  <div class='inputs_emprunt'>
                    

                    <div class='ref_doc'>  
                      <div class='label'>Réference :</div> <input type='text' name='doc' placeholder='Réference Document'/>
                      <input type='submit' name='push' value='+' />
                      <input type='hidden' value='c_push' name='action'/>

                    </div></form>
                    <form method='post' action=''><div class='ref_adherent'>  
                      <div class='label'>N°Adhérent :</div> <input type='text' name='ad' placeholder='N°Adhérent'/>
                    </div>
                  </div>";
                  if(isset($_SESSION['doc'])){
                    
                  if(isset($_POST['emprunt_recap']))
                  $html.="<form method='post' action=''><div class='list_ref' style='display:none;' >";
                  else
                  $html.="<div class='list_ref' >";
                  $html.="<select name='list[]' multiple size='6' >";
                  
                  foreach ($_SESSION['doc'] as $doc) {

                  $html.="<option selected value='".$doc->id."'>".$doc->title."</option>";
                   } 
                  
                  $html.="</select>
                  </div>";
                  }

                  $html.="<div class='bouton'>
                    <input type='submit' name='emprunt_recap' value='Valider' />
                    <input type='hidden' name='action' value='c_emprunt_recap'/>
                  </div>
                  </form>
                </fieldset>
             </div>";
             if(isset($_GET['recap'])){
              $html.= $this->emprunt_recap()."</div>";
             }
    return $html;
  }

  public function emprunt_recap(){

   $this->loan=$_SESSION['loan'];
    $result = count($this->loan);
    $html="<div class='add_emprunt'>
            <div class='recap'> 
              <div class='recap_title'>
                <h4>Récapitulatif</h4>
              </div>

              <div class='recap_details'>
                <div class='nb'>
                  <strong>Nombre de document :</strong> <span>".$result."</span>
                </div>
              </div>

              <div class='tab'>
                <table>
                  <tr>
                    <th>Réference</th>
                    <th>Titre</th>
                    <th>Type</th>
                    <th>Genre</th>
                    <th>date retour</th>
                  </tr>";
                  
                  foreach ($this->loan as $l) {
                     $html.="               
                  <tr>
                    <td>".$l->ref."</td>
                    <td>".$l->title."</td>
                    <td>".$l->name."</td>
                    <td>".$l->category."</td>
                    <td>".$l->return_date."</td>
                  </tr>";
                  }
                 
                $html.="</table>
              </div>

              <div class='print'>
                <input type='submit' value='Imprimer' />
              </div>

            </div>
          </div>
          ";
    session_destroy();
    return $html;
  }

  

  public function return_staff(){
    session_start();

    $html="<div class='content_emprunt'>
             <div class='add_emprunt'>
                <fieldset>
                  <legend>Retour des préts</legend>
                  <form method='post' action=''>
                  <div class='inputs_emprunt'>
                      <div class='ref_doc'>  
                      <div class='label'>Réference :</div> <input name='ref' type='text' placeholder='Réference Document'/>
                      <input type='submit' name='pull' value='-' />
                      <input type='hidden' value='c_pull' name='action'/>

                    </div>
                  </div></form>";
                  if(isset($_SESSION['doc'])){
                   $html.="<form method='post' action=''>";
                  if(isset($_POST['return_recap']))
                  $html.="<div class='list_ref' style='display:none;' >";
                  else
                  $html.="<div class='list_ref' >";
                  $html.="<select size='6' name='doc_ref[]' multiple>";
                  
                  foreach ($_SESSION['doc'] as $doc) {

                  $html.="<option selected value='".$doc->id."'>".$doc->title."</option>";
                   } 
                  
                  $html.="</select>
                  </div>";
                  }
                 $html.="<div class='bouton'>
                    
                    <input type='submit' name='return_recap' value='Valider' />
                    <input type='hidden' value='c_return_recap' name='action'/>


                  </div>
                  </form>
                </fieldset>

             </div>";
            if(isset($_GET['recap'])){
              $html.= $this->return_recap()."</div>";
             }

    return $html;
  }

  public function return_recap(){
    
    if (isset($_SESSION['loan_restant']))
    $this->loan_restant=$_SESSION['loan_restant'];

  if (isset($_SESSION['loan_rendu']))
    $this->loan_rendu=$_SESSION['loan_rendu'];

    $html="<div class='add_emprunt'>
            <div class='recap'> 
              <div class='recap_title'>
                <h4>Récapitulatif</h4>
              </div>

              <div class='recap_details'>
                <div class='nb'>
                  <strong>Nombre de document rendus :</strong> <span>".count($this->loan_rendu)."</span>
                </div>
              </div>

              <div class='tab'>
                <table>
                  <tr>
                    <th>Réference</th>
                    <th>Titre</th>
                    <th>Type</th>
                    <th>Genre</th>
                  </tr>";


                   if(count($this->loan_rendu)==1){
                   
                    $doc=$this->loan_rendu;

                      $html.="<tr>
                            <td>".$doc->ref."</td>
                            <td>".$doc->title."</td>
                            <td>".$doc->name."</td>
                            <td>".$doc->category."</td>
                        </tr>";      
                   
                 }


                  if(count($this->loan_rendu)>1){
                    foreach ($this->loan_rendu as $doc) {
                      
                       $html.="<tr>
                         <td>".$doc->ref."</td>
                         <td>".$doc->title."</td>
                         <td>".$doc->name."</td>
                         <td>".$doc->category."</td>
                         </tr>";      
                       }
                     }
                              
                $html.="</table>
              </div>";

              $html.="<div class='nb'>
                  <strong>Nombre de document restants :</strong> <span>".count($this->loan_restant)."</span>
                </div>
              

              <div class='tab'>
                <table>
                  <tr>
                    <th>Réference</th>
                    <th>Titre</th>
                    <th>Type</th>
                    <th>Genre</th>

                    <th>date retour</th>
                  </tr>";
                
                // if(count($this->loan_restant)>0){
                //   $doc=$this->loan_restant;
                
                // $html.="<tr>
                //       <td>".$doc->ref."</td>
                //       <td>".$doc->title."</td>
                //       <td>".$doc->type."</td>
                //       <td>".$doc->category."</td>
                //       <td>".$doc->return_date."</td>
                //   </tr>";      
                    
                //   }


                  if(count($this->loan_restant)>0){

                foreach ($this->loan_restant as $doc) {
                $html.="<tr>
                      <td>".$doc->ref."</td>
                      <td>".$doc->title."</td>
                      <td>".$doc->type."</td>
                      <td>".$doc->category."</td>
                      <td>".$doc->return_date."</td>
                  </tr>";      
                    } 
                  }


                $html.="</table>
              </div>

              <div class='print'>
                <input type='submit' value='Imprimer' />
              </div>

            </div>
          </div>
          ";
    session_destroy();
    return $html;
  }

/*******************Aditionnel*********************/
/**************Doc*****************/
  public function insert_type(){
    

     $html="<div class='add_adherent'>
    <div class='f_titre'>
          <h4>Type - Document</h4>
    </div>
      <form action='indexStaff.php' method='post'>
        <div id='form'>
          
            <div class='input_ad'>
              <div class='label'>Nouvel Type :</div> <input type='text' name='nom'/>
              <input type='submit' value='Ajouter' name='add_type'/>
               <input type='hidden' value='add_type' name='action'/>
          </div>
        
        </div>
        </form>
        </div>";

    
    return $html;
  }

   public function insert_category(){
    

     $html="<div class='add_adherent'>
    <div class='f_titre'>
          <h4>Genre - Document</h4>
    </div>
      <form action='indexStaff.php' method='post'>
        <div id='form'>
            <div class='input_ad'>
              <div class='label'>Nouvel Genre :</div> <input type='text' name='nom'/>
              <input type='submit' value='Ajouter' name='add_category'/>
              <input type='hidden' value='c_add_category' name='action'/>
          </div>
        
        </div>
        </form>
        </div>";

    
    return $html;
  }
public function insert_adherent(){
    $html="<div class='add_adherent'>
    <div class='f_titre'>
          <h4>Formulaire d'insertion</h4>
        </div>
      <form action='indexStaff.php' method='post'>

        <div class='f1'>
          <div class='inputs'>
            <div class='label'>Nom :</div> <input type='text' name='nom'/>
          </div>

          <div class='inputs'>
            <div class='label'>Prénom :</div> <input type='text' name='prenom'/>
          </div>

          <div class='inputs'>
            <div class='label'>mail :</div> <input type='text' name='email'/>
          </div>

          <div class='inputs'>
            <div class='label'>mobile :</div> <input type='text' name='mobile'/>
          </div>
        </div>
        
        <div class='f2'>
        <div class='inputs >
          <div class='label'>Adresse :</div> <textarea name='address' cols='20' rows='6'></textarea>
          <div class='bouton'>
          <input type='submit' value='Valider' name='insert_ad'/>
          <input type='hidden' value='c_insert_ad' name='action'/>
          </div>
        </div>
        
        </div>

        </form>
        </div>";
    return $html;
  }

  public function update_adherent(){
    session_start();
    if (isset($_SESSION['adherent'])) $r=$_SESSION['adherent']->ref; else $r="";

    $html="<div class='add_adherent'>
    <div class='f_titre'>
          <h4>Formulaire de modification</h4>
        </div>
      <form action='' method='post'>
        
        <div class='f_adherent'>
          <div class='input_ad'>
            <div class='label'>Réference Adhrent :</div> <input type='text' value='".$r."' name='ref'/>
            <input type='submit' value='Afficher' name='view'/>
            <input type='hidden' value='c_view_update_ad' name='action'/>

              
          </div>
        </div>";
        
        if(isset($_GET['view'])){
          

          $this->adherent=$_SESSION['adherent'];
           

        $html.="<div id='form'>
          <div class='f1'>
            <div class='inputs'>
              <div class='label'>Nom :</div> <input type='text' value='".$this->adherent->lastname."' name='nom'/>
            </div>

            <div class='inputs'>
              <div class='label'>Prénom :</div> <input type='text' value='".$this->adherent->firstname."' name='prenom'/>
            </div>

            <div class='inputs'>
              <div class='label'>mail :</div> <input type='text' value='".$this->adherent->email."' name='email'/>
            </div>

            <div class='inputs'>
              <div class='label'>mobile :</div> <input type='text' value='".$this->adherent->phone."' name='mobile'/>
            </div>
          </div>
          
          <div class='f2'>
          <div class='inputs >
            <div class='label'>Adresse :</div> <textarea cols='20' name='address' rows='6'>".$this->adherent->adress."</textarea>
            <div class='bouton'>
            <input type='submit' value='Modifier' name='update_ad'/>
            <input type='hidden' value='c_update_ad' name='action'/>
            </div>
          </div>
        </div>
          </div>

        </form>";
      }
        $html.="</div>";

    return $html;
  }

public function delete_adherent(){
  $html="<div class='add_adherent'>
    <div class='f_titre'>
          <h4>Formulaire de Suppression</h4>
    </div>
      <form action='indexStaff.php' method='post'>
        <div id='form'>
          
            <div class='input_ad'>
              <div class='label'>Réference Adhrent :</div> <input type='text' name='ref'/>
              <input type='submit' value='Supprimer' name='delete_ad'/>
                <input type='hidden' value='c_delete_ad' name='action'/>
          </div>
        
        </div>
        </form>
        </div>";
    return $html;

}

  public function insert_doc(){
    $html="<div class='add_adherent'>
    <div class='f_titre'>
          <h4>Document - Insertion</h4>
        </div>
      <form action='indexStaff.php' method='post'>

        <div class='f1'>
          <div class='inputs'>
            <div class='label'>Ref :</div> <input type='text' name='ref'/>
          </div>

          <div class='inputs'>
            <div class='label'>Titre :</div> <input type='text' name='titre'/>
          </div>

          <div class='inputs'>
            <div class='label'>Descriprion :</div> <textarea name='description' cols='18' rows='2'></textarea>
          </div>

          <div class='inputs'>
            <div class='label'>Edition :</div> <input type='text' name='edition'/>
          </div>
        
        

        </div>

        <div class='f2 f22'>
        <div class='inputs'>
        <div class='label'>Genre :</div> <select name='category'><option>Choisir Genre</option>";
                  foreach ($this->category as $c) {
                     $html.= " <option  value='".$c->id."'>".$c->name."</option>";
                   }
                   $html.="
                    </select>
        </div>

        <div class='inputs'>
        <div class='label'>Type :</div> <select name='type'><option>Choisir Type</option>";
                  foreach ($this->type as $c) {
                     $html.= " <option  value='".$c->id."'>".$c->name."</option>";
                   }
                   $html.="
                    </select>
        </div>
            <div class='inputs'>
              <div class='label'>Image :</div> 
                <input type='file' name='image' accept='image/*'/>
          </div>
        <div class='inputs' >
          <input type='submit' value='Valider' name='c_insert_doc'/>
          <input type='hidden' value='c_insert_doc' name='action'/>
        
        </div>

        
        </div>

        </form>
        </div>";
    return $html;
  }

  public function delete_doc(){
  $html="<div class='add_adherent'>
    <div class='f_titre'>
          <h4>Document - Suppression</h4>
    </div>
      <form action='indexStaff.php' method='post'>
        <div id='form'>
          
            <div class='input_ad'>
              <div class='label'>Réference Document :</div> <input type='text' name='ref'/>
              <input type='hidden' value='c_delete_doc' name='action'/>
              <input type='submit' value='Supprimer' name='delete_doc'/>
              
          </div>
        
        </div>
        </form>
        </div>";
    return $html;

}

  

  public function afficheGeneral($sel){

        switch ($sel){

          case 'home_staff':{
            $this->centre=$this->home_staff();
            break;
          }

           case 'emprunt_staff':{
            $this->centre=$this->emprunt_staff();
            break;
          }
          case 'emprunt_recap':{
            $this->centre=$this->emprunt_staff();
            break;
          }
          case 'return_staff':{
            $this->centre=$this->return_staff();
            break;
          }

          case 'insert_adherent':{
            $this->centre=$this->insert_adherent();
            break;
          }

           case 'update_adherent':{
            $this->centre=$this->update_adherent();
            break;
          }

           case 'delete_adherent':{
            $this->centre=$this->delete_adherent();
            break;
          }

          case 'insert_type':{
            $this->centre=$this->insert_type();
            break;
          }

          case 'insert_category':{
            $this->centre=$this->insert_category();
            break;
          }

          case 'insert_doc':{
            $this->centre=$this->insert_doc();
            break;
          }
          case 'delete_doc':{
            $this->centre=$this->delete_doc();
            break;
          }
            case 'gestion_ad':{
            $this->centre=$this->gestion_ad();
            break;
          }

          case 'gestion_doc':{
            $this->centre=$this->gestion_doc();
            break;
          } 
           
          

        }


echo "
<!DOCTYPE html>
<html lang=\"fr\">
    <head>
        <meta  charset=\"utf-8\"/>
        <title>Ma-Bibliothèque</title>        
        
        <link rel='stylesheet' href='stylesheets/reset.css'/>
        <link rel='stylesheet' href='stylesheets/framework.css'/>
        <link rel='stylesheet' href='stylesheets/staff.css'/>
        
        
    </head>
<body>\n";

echo"
<div id='content'>\n";

echo"
<header>
  <div class='header'>  
    <div class='title_bib'>  
      <h2>MaBibliothèque</h2> <img src='stylesheets/img/logo.png' width='100' />       
    </div>
    <div class='barre'>
     <div class='menu_staff'><a href='indexStaff.php'><h4>Accueil</h4></a></div>
        <div class='menu_staff'><a href='indexStaff.php?action=gestion_ad'><h4>Gestion Adhérents</h4></a></div>
        <div class='menu_staff'><a href='indexStaff.php?action=gestion_doc'><h4>Gestion Documents</h4></a></div>  
    </div>
  </div>
</header>\n";

echo "<nav>";
echo "</nav>";


echo"<article>";

echo"</article>";

echo"<aside>";

echo"</aside>";

echo "<section class='home_search'>";
echo $this->centre;
echo "</section>";


echo"<footer><div class='footer'> <h2>Copyright 2014</h2></div></footer>";
echo"
</div>\n";
echo "</body>";
echo "</html>";

  }


}