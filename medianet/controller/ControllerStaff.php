<?php 

class ControllerStaff{

	
	
	public function c_emprunt_recap(){
		
			if(isset($_POST['emprunt_recap'])){

					$get=$_POST;
					$v=new ViewStaff();
					$l=new Loan();
					$d=new Document();
					$a=Adherent::FindByRef($get['ad']);
					
					$l->__set('id_adherent',$a->id);

					foreach($get['list'] as $cle => $valeur) {
						
						$d=Document::FindById($valeur);
						
						if($d->id_state==1){
						
						  $l->__set('id_document',$valeur);
						  $l->insert();
						  $l->updateState();
						}
						  
					}
					session_start();
					$_SESSION['loan']=Loan::getAllLoans($a->id);

					
					header("location: indexStaff.php?action=emprunt_staff&recap=on");
				}

		}

	public function push(){
				
				if (isset($_POST['push'])){

					session_start();
					
					

					$d=new Document();
					$doc=$d->findByRef($_POST['doc']);
					if (!isset($_SESSION['doc'])) {
						$_SESSION['doc']=array();
					}
					if($doc)
					array_push($_SESSION['doc'], $doc);
					header("location: indexStaff.php?action=emprunt_staff");
					
				}
		}

	public function add_type(){

				if(isset($_POST['add_type'])){
				$t=new Type();
				$t->__set('name',$_POST['nom']);
				$t->insert();
				header("location: indexStaff.php?action=gestion_doc");
				}
		}

	public function add_category(){

				if(isset($_POST['add_category'])){
					$c=new Category();
					$c->__set('name',$_POST['nom']);
					$c->insert();
					header("location: indexStaff.php?action=gestion_doc");
				}
		}

	public function pull(){
				
				if(isset($_POST['pull'])){

					session_start();
					
					$d=new Document();
					$doc=$d->findByRef($_POST['ref']);
					
					if (!isset($_SESSION['doc'])) {
						$_SESSION['doc']=array();
					}
					if($doc)
					array_push($_SESSION['doc'], $doc);

				header("location: indexStaff.php?action=return_staff");
					
				}
		}

	public function return_recap(){
				
				if(isset($_POST['return_recap'])){

					$c=new Category();
					$t=new Type();
					$d=new Document();
					$l=new Loan();

					session_start();
					$get=$_POST;
					
					foreach ($_SESSION['doc'] as $doc) {
						$d->updatesState($doc->ref);
	                 } 
	                 
					
					$ad=Loan::getAdherent($_SESSION['doc'][0]->ref);
					
					foreach ($get['doc_ref'] as $doc) {
							
				      $l->deleteLoan($doc);
										
					}	

						$docs=Loan::getDocumentRestant($ad);
						
						$_SESSION['loan_restant']=$docs;
						
						$d2=array();
						if(count($_SESSION['doc'])==1){
							foreach ($_SESSION['doc'] as $doc) {
							$_SESSION['loan_rendu']=$d->getDocumentRendus($doc->ref);

						}
						}else{
							foreach ($_SESSION['doc'] as $doc) {
							  $d2[]=$d->getDocumentRendus($doc->ref);

						}

						$_SESSION['loan_rendu']=$d2;
						}
						
						 header("location: indexStaff.php?action=return_staff&recap=on");
						 
				}
		}


	public function insert_ad(){
		
			if (isset($_POST['insert_ad'])) {
					$ad=new  Adherent();
					 $ad->__set('lastname',$_POST['nom']);
					 $ad->__set('firstname',$_POST['prenom']);
					 $ad->__set('phone',$_POST['mobile']);
					 $ad->__set('address',$_POST['address']);
					 $ad->__set('email',$_POST['email']);
					if($ad->insert())
						header("location: indexStaff.php?action=gestion_ad");
					
				}
	}

	public function view_update_ad(){

		if(isset($_POST['view'])){
			$v = new ViewStaff();
			$v->adherent=Adherent::FindByRef($_POST['ref']);
			session_start();
			$_SESSION['adherent']=$v->adherent;
			
			header("location: indexStaff.php?action=update_adherent&view=on");
				
		}
	}
	
	public function update_ad(){

		if (isset($_POST['update_ad'])) {
				$ad=new  Adherent();
				 $ad->__set('ref',$_POST['ref']);
				 $ad->__set('lastname',$_POST['nom']);
				 $ad->__set('firstname',$_POST['prenom']);
				 $ad->__set('phone',$_POST['mobile']);
				 $ad->__set('address',$_POST['address']);
				 $ad->__set('email',$_POST['email']);


				 if($ad->update())
					header("location: indexStaff.php?action=gestion_ad");
				
		}
	}

	public function delete_ad(){

		if (isset($_POST['delete_ad'])) {
				$ad=new  Adherent();
				 $ad->__set('ref',$_POST['ref']);
				 
				 if($ad->delete())
					header("location: indexStaff.php?action=gestion_ad");
				
		}
	}

	public function insert_doc(){
		
		if(isset($_POST['c_insert_doc'])){

				$get=$_POST;
				$d=new Document();
				$d->__set('ref',$get['ref']);
				$d->__set('title',$get['titre']);
				$d->__set('description',$get['description']);
				$d->__set('edition',$get['edition']);
				$d->__set('image',$get['image']);
				$d->__set('id_category',$get['category']);
				$d->__set('id_type',$get['type']);
				if($d->insert())
				header("location: indexStaff.php?action=gestion_doc");
			}
	}


	public function delete_doc(){
	
			if (isset($_POST['delete_doc'])) {
					
					$doc=new  Document();
					 $doc->__set('ref',$_POST['ref']);
					 
					 if($doc->delete())
						header("location: indexStaff.php?action=gestion_doc");


					
			}
		}






	public function dispatch($get){

			
			
			$v = new ViewStaff();
			$a = new Adherent();
			$l = new Loan();
			$d1 = new Document();
			$c = Category::findAll();
			$t=Type::findAll();


			if ($_POST) {
				$get=$_POST;
			}
			
			$v->category=$c;
			$v->type=$t;


		

     if ($get){
        switch ($get['action']){

			/******Emprunt staff********/
          case 'emprunt_staff':{
            $v->afficheGeneral('emprunt_staff');
            break;
          }
          		
          		/*emprunt recap*/
          case 'c_emprunt_recap':{
            $this->c_emprunt_recap();
            break;
          }
          		/*push*/
          case 'c_push':{
          	
            $this->push();
            break;
          }
          
          /****************************/

          
          /******return staff********/
          case 'return_staff':{
            $v->afficheGeneral('return_staff');
            break;
          }
          		/*return recap*/
          case 'c_return_recap':{

            $this->return_recap();
            break;
          }
          		/*pull*/
          case 'c_pull':{
            $this->pull();
            break;
          }

          /****************************/


          /******Insert Ad********/
          case 'insert_adherent':{
            $v->afficheGeneral('insert_adherent');
            break;
          }
          
          case 'c_insert_ad':{
            $this->insert_ad();
            break;
          }

         /****************************/


          /******Update Ad********/
           case 'update_adherent':{
            $v->afficheGeneral('update_adherent');
            break;
        	}
       
        		/*view*/
        	case 'c_view_update_ad':{
            $this->view_update_ad();
            break;
          }
          		/*update*/
            case 'c_update_ad':{
            $this->update_ad();
            break;
          }
          
          /****************************/

          
          /******Delete Ad********/
           case 'delete_adherent':{
            $v->afficheGeneral('delete_adherent');
            break;
          }

          case 'c_delete_ad':{
            $this->delete_ad();
            break;
          }

          /****************************/

          
          /******insert Type********/
          case 'insert_type':{
            $v->afficheGeneral('insert_type');
            break;
          }

          case 'add_type':{
            $this->add_type();
            break;
          }
          
          /****************************/


          /******insert Category********/
          case 'insert_category':{
            $v->afficheGeneral('insert_category');
            break;
          }

          case 'c_add_category':{
            $this->add_category();
            break;
          }

          /****************************/
          

          /******insert doc********/
          case 'insert_doc':{
            $v->afficheGeneral('insert_doc');
            break;
          }

          case 'c_insert_doc':{
            $this->insert_doc();
            break;
          }

          /****************************/


          /******Delete doc********/
           case 'delete_doc':{
            $v->afficheGeneral('delete_doc');
            break;
          }
          case 'c_delete_doc':{
            $this->delete_doc();

            break;
          }
          
          
          /****************************/


          /******Gestion ad********/
            case 'gestion_ad':{
            $v->afficheGeneral('gestion_ad');
            break;
          }
          
          /****************************/


          /******Gestion doc********/
          case 'gestion_doc':{
            $v->afficheGeneral('gestion_doc');
            break;
         } 
         
         /****************************/
			

          	

         }

       }
    
    else
	$v->afficheGeneral('home_staff');		
			

									
	}


}