<?php 

class ControllerAdherent{

	public function search(){

			if(isset($_POST['search'])){
				 	
				 	$get=$_POST;
				 	$v=new ViewAdherent();
				 	$d=new Document();

				 	$v->titre_doc=$get['motclé'];
				 	if($get['word']=='titre'){

					 $d->__set('title',$get['motclé']);  
				 	}
				 	if($get['word']=='description'){

					$d->__set('description',$get['motclé']); 
				 	}
				 	if(isset($get['type'])){
					$d->__set('id_type',$get['type']); 
				 	}
				 	if((isset($get['category']))&& ($get['category']!='Choisir Catégorie')){

					$d->__set('id_category',$get['category']); 
				 	}
					$doc=$d->SearchDocument();
					
					$v->document=$doc;

					$_SESSION['document']=$doc;

					header("location: indexAdherent.php?action=search");
			}
	}

	public function search_details(){
		
			$get=$_GET;
			if(isset($get['ref'])){
					
				$v=new ViewAdherent();
				$d=new Document();
				$d=Document::FindByRef($get['ref']); 
				
				if($d->id_state==1){
					$_SESSION['document']=$d;
				}
				else{
					$d=Document::FindByRefE($get['ref']);
					$_SESSION['document']=$d;
					}
				
				header("location: indexAdherent.php?action=search_details");
				}



	}

	public function loan_adherent(){
		


					$ad=Adherent::findByUserName($_SESSION['username']);
					
					$docs=Loan::getAllLoans($ad->id_adherent);

					$_SESSION['loan']=$docs;

					header("location: indexAdherent.php?action=loan_adherent");


		 }

		 public function logout(){	
				
					$s = new Session();
					$s->logout();
					header("location: indexAdherent.php?action=Login_form");
				
			}


		public function dispatch($get){	
			
			$v = new ViewAdherent();
			$c = Category::findAll();
			$t=Type::findAll();
			$d=new Document();

			session_start();


			if ($_POST)
				$get=$_POST;

			
			$v->category=$c;
			$v->type=$t;


     if ($get){
        switch ($get['action']){

			/******Home Adherent********/
          case 'accueil':{
            $v->afficheGeneral('accueil');
            break;
          }
          		
          
          /****************************/

          
          /******Search Doc********/
          case 'search':{
            $v->afficheGeneral('search');
            break;
          }
          case 'c_search':{
            $this->search();
            break;
          }
          // 		/*pull*/
          // case 'c_pull':{
          //   $this->pull();
          //   break;
          // }

          /****************************/


          /******Search Details********/
          case 'search_details':{
            $v->afficheGeneral('search_details');
            break;
          }
          case 'c_search_details':{
            $this->search_details();
            break;
          }
          

         /****************************/

		/******Login Form********/
          case 'Login_form':{
            $v->afficheGeneral('Login_form');
            break;
          }

          /****************************/

         /******Loan Adherent********/
          case 'loan_adherent':{
            $v->afficheGeneral('loan_adherent');
            break;
          }
          case 'c_loan_adherent':{
            $this->loan_adherent();
            break;
          }

          /****************************/


          /******Logout*********/
           case 'logout':{
            $this->logout();
            break;
          }

          
         
         /****************************/
			

          	

         }

       }
    
    else
	$v->afficheGeneral('accueil');		
			

									
	}



	
}
