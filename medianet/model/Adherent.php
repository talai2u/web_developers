<?php 
class Adherent{

	private $id;
	private $ref; 
	private $lastname;
	private $firstname;
	private $phone;
	private $address;
	private $email;

    public function __construct() {
    
    }
    
     public function __toString() {
        return "[". __CLASS__ . "] id : ". $this->id . ":
				   titre  ". $this->titre  .":
				   body ". $this->body .":
           cat_id ". $this->cat_id .":
           date ". $this->date ;
  }

  


  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }
   

  
  
    public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }
public function insert() {
  
  $db=Connexion::getConnection();   
  $datedebut=date("Y-m-d");
   $save_query = "insert into adherent (ref,firstname,lastname,phone,adress,email,dateAdhesion)
     values('".rand(5,15)."','".$this->__get('firstname')."','".$this->__get('lastname')."','".$this->__get('phone')."',
      '".$this->__get('address')."','".$this->__get('email')."','".$datedebut."')" ;
    try{     

        $aff_rows = $db->exec($save_query);
         $this->__set('id',$db->LastInsertId('adherent'));
         
        return $aff_rows;
    }
    catch(BaseException $e){ 
      throw new PDOException("Error insert".$e->getMessage(). '<br/>');
    }
    
  
  
  }
  public function update() {

    if (!isset($this->ref)) {
    echo " unsset id";
    throw new Exception(__CLASS__ . ": Reference undefined : cannot update");
    }

    $save_query = "update adherent set firstname='".$this->firstname."',
    lastname='".$this->lastname."',
    phone='".$this->phone."',
    email='".$this->email."',
    adress='".$this->address."' where ref='".$this->ref."'";
    $pdo = Connexion::getConnection();
    $nb=$pdo->exec($save_query);
    
    if(isset($nb)) 
    return $nb;
    else
    return false;

  }
  public function findByUserName($username){
    try {
    $query="select * from connexion where username='".$username."'";

    $db=Connexion::getConnection();
    $dbres= $db->query($query);
    
    $u=$dbres->fetch(PDO::FETCH_OBJ);
    } catch (Exception $e) {
      echo $e->message();
    }
    

    if(isset($u->id))
      return $u;
    else
      return false;

  }
   public function delete() {
    
    $db=Connexion::getConnection();   

     if (!isset($this->ref)) {
      throw new Exception(__CLASS__ . ": Reference undefined : cannot update");
    } 

   $save_query = "delete from adherent where ref='".$this->ref."'" ;
    try{     
        $exec=$db->prepare($save_query);
        $exec->execute();
        
        return $exec->rowCount();
    }
    catch(BaseException $e){ 
      throw new PDOException("Error Delete".$e->getMessage(). '<br/>');
    }
   
}
  public static function FindByRef($reference){
     $query = "select * from adherent where ref = '".$reference."'" ;
      
      $db = Connexion::getConnection();
      $dbres = $db->query($query);
      
      if($dbres){
      $a=$dbres->fetch(PDO::FETCH_OBJ) ;
    }
      if(isset($a->id)){
      return $a;  
      }
      else{
        return false;
      }
  }





}






 ?>