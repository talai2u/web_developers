<?php 

class Document{
	
	private $id;
	private $ref;
	private $title;
	private $description;
	private $edition;
	private $id_state;
	private $id_category;
	private $id_type;
  private $image;


	public function __construct() {
    
  	}



  	public function __get($attr_name) {
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
	    throw new Exception($emess, 45);
  	}

  	public function __set($attr_name, $attr_val) {
	    if (property_exists( __CLASS__, $attr_name)) {
	      $this->$attr_name=$attr_val; 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
	    throw new Exception($emess, 45);
    
  }
public function insert() {
  $state=1; 
   $save_query = "insert into document (ref,title,description,edition,id_state,id_category,id_type,image)
     values('".$this->__get('ref')."','".$this->__get('title')."','".$this->__get('description')."','".$this->__get('edition')."',
      ".$state.",".$this->__get('id_category').",".$this->__get('id_type').",'".$this->__get('image')."')" ;
      
      $db=Connexion::getConnection();  
      try{     

        $aff_rows = $db->exec($save_query);
         $this->__set('id',$db->LastInsertId('document'));
         
        return $aff_rows;
    }
    catch(BaseException $e){ 
      throw new PDOException("Error insert".$e->getMessage(). '<br/>');
    }
    
  }
  public static function FindByRef($reference){
  	 
     $query = "select * from document where ref = '".$reference."'" ;
    
      
      $db = Connexion::getConnection();
      $dbres = $db->query($query);
      
      if($dbres){
      $doc=$dbres->fetch(PDO::FETCH_OBJ) ;
  	}
      if(isset($doc)){
      return $doc;  
      }
      else{
        return false;
      }
  }

  public static function FindById($id){
     
     $query = "select * from document where id = ".$id."" ;
      
      $db = Connexion::getConnection();
      $dbres = $db->query($query);
      
      if($dbres){
      $doc=$dbres->fetch(PDO::FETCH_OBJ) ;
    }
      if(isset($doc)){
      return $doc;  
      }
      else{
        return false;
      }
  }
   public static function FindByRefE($reference){
     $query = "select title,description,edition,return_date,name,id_state,id_type,image from document,loan,state
      where document.id_state=state.id and document.id=loan.id_document 
      and document.ref='".$reference."'" ;
      
      $db = Connexion::getConnection();
      $dbres = $db->query($query);
      
      if($dbres){
      $doc=$dbres->fetch(PDO::FETCH_OBJ) ;
    }
      if(isset($doc)){
      return $doc;  
      }
      else{
        return false;
      }
  }
  

  
  public function SearchDocument(){

        $sql="select * from document where ref is not null and ";
        
        if(isset($this->title)){
                $sql.="title LIKE '". $this->title."%'  and ";
        }

        if(isset($this->description)){
                $sql.="description LIKE '". $this->description."%' and ";
        }

        if(isset($this->id_type)){
                $sql.="id_type=". $this->id_type." and ";
        }

        if(isset($this->id_category)){
                $sql.="id_category =". $this->id_category ." and ";
        }

        $sql.="id > 0 order by id";

        $db = Connexion::getConnection();
        $dbres = $db->query($sql);
     
        $b = $dbres->fetchALL(PDO::FETCH_OBJ);
    
        if(isset($b)){
        return $b;
       
        }
      else{
        return false;
      }

  }
  public function updatesState($ref) {

    
    $save_query = "update document set id_state=1
                                    where ref='".$ref."'";

    
    $pdo = Connexion::getConnection();
    $nb=$pdo->exec($save_query);
    if(isset($nb))
      return $nb;
    else
      return false;
   
    
  }
  public function getDocumentRendus($ref){
    $sql="select ref ,title , type.name ,category.name as 'category' from document,type,category where document.id_type=type.id and document.id_category=category.id and document.ref='".$ref."'";
    $db = Connexion::getConnection();
        $dbres = $db->query($sql);
        $b = $dbres->fetch(PDO::FETCH_OBJ);
        if(isset($b))
          return $b;
        else
          return false;
       
        
  }
   
  public function delete() {
    
    $db=Connexion::getConnection();   
   $save_query = "delete from document where ref='".$this->ref."'" ;
    try{     
        $exec=$db->prepare($save_query);
        $exec->execute();
        
        return $exec->rowCount();
    }
    catch(BaseException $e){ 
      throw new PDOException("Error Delete".$e->getMessage(). '<br/>');
      }
  }

}