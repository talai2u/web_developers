<?php 
class Session{
	private $logged_in=false;
	private $username;
	function __construct() {
		$this->check_login();
	}
	public function is_logged_in() {
		return $this->logged_in;
	}
	private function check_login() {
		if(isset($_SESSION['username'])) {
		$this->username = $_SESSION['username'];
		$this->logged_in = true;
		} else {
		unset($this->username);
		$this->logged_in = false;
		}
	}

	public function login($user, $pwd) {
		$u=Adherent::findByUserName($user);
		// $hash=crypt($pass,$u->salt);
		// database should find user based on username/password
		// pass should be stored using crypt() in DB
		// if($hash==$u->password)
		if($pwd == $u->pwd){
			session_start();
			$this->username = $_SESSION['username'] = $user;
			$this->logged_in = true;
		}
		return $this->logged_in;
	}
	public function logout() {
		unset($_SESSION['username']);
		unset($this->username);
		$this->logged_in = false;
		session_destroy();
	}

}
?>