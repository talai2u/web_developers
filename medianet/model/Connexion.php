<?php 

class Connexion{
	
	private static $db_link;

	

	private static function connect(){
		
		


		Try {
				$ini_array = parse_ini_file("config.ini");
				
				$dsn="mysql:host=".$ini_array['hostname'].";dbname=".$ini_array['dbname'];
				$user = $ini_array['user'];
				$pass=$ini_array['password'];
				$db = 	new PDO($dsn, $user,$pass,
						array(PDO::ERRMODE_EXCEPTION=>true,
						PDO::ATTR_PERSISTENT=>true));
				$db->exec("SET CHARACTER SET utf8");
				
				self::$db_link=$db;
				
	
			} catch(PDOException $e) {
						throw new PDOException("connection error: $dsn".$e->getMessage(). '<br/>');
				}
	} 

	public static function getConnection(){
		if (isset($db_link)) {
			return self::$db_link;
		}
		else{
			self::connect();
			return self::$db_link;
		}
	}	
}


?>