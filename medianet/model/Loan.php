<?php 

class Loan{
  
  private $id;
  private $loaning_date;
  private $return_date;
  private $id_adherent;
  private $id_document;


  public function __construct() {
    
    }



    public function __get($attr_name) {
      if (property_exists( __CLASS__, $attr_name)) { 
        return $this->$attr_name;
      } 
      $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
      throw new Exception($emess, 45);
    }

    public function __set($attr_name, $attr_val) {
      if (property_exists( __CLASS__, $attr_name)) {
        $this->$attr_name=$attr_val; 
        return $this->$attr_name;
      } 
      $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
      throw new Exception($emess, 45);
  }
  public function insert(){   
  $datedebut=date("Y-m-d");
 $datefin=30;
 $d=new Document();
 $datefin = date('Y-m-d', strtotime($datedebut.' +'.$datefin.' days'));
 $save_query = "insert into loan (loaning_date,return_date,id_adherent,id_document)  
      values('".$datedebut."','".$datefin."',".$this->__get('id_adherent').",".$this->__get('id_document').")" ;
    try{     
        
        $db=Connexion::getConnection();
        $aff_rows = $db->exec($save_query);
         $this->__set('id',$db->LastInsertId('loan'));
        return $aff_rows;
    }
    catch(BaseException $e){ 
      throw new PDOException("Error insert".$e->getMessage(). '<br/>');
    }

  }
  public function updateState(){
        $state=2;
    $save_query = "update document set id_state=".$state."
                                 where id='".$this->id_document."'";
    // // echo $save_query;
     $db=Connexion::getConnection();
    $aff_rows = $db->exec($save_query);
    $nb=$db->exec($save_query);
    
  }
  public static function getAllLoans($id_adherent){
  $query = "select document.ref,title,category.name as 'category',type.name,return_date 
            from document, adherent,loan,type,category 
            where document.id=loan.id_document and loan.id_adherent=adherent.id and document.id_type= type.id and document.id_category=category.id 
            and loan.id_adherent=".$id_adherent."";
  $pdo=Connexion::getConnection();
    
  $dbres=$pdo->query($query);
  $c=$dbres->fetchall(PDO::FETCH_OBJ);

  if(isset($c))
    return $c;
  else
    return false;
 }
 public static function getAdherent($ref){
  $query="select loan.id_adherent from loan,document 
          where loan.id_document=document.id 
          and document.ref='".$ref."'";
   $pdo=Connexion::getConnection();
  
  $dbres=$pdo->query($query);
  $c=$dbres->fetch(PDO::FETCH_OBJ);
  if($c){
    return $c->id_adherent;
  }
 }
 public static function deleteLoan($doc){
   
  $query="delete from loan where id_document=".$doc;
 
  try{  
       $db=Connexion::getConnection();    
        $exec=$db->prepare($query);
        $exec->execute();
        return $exec->rowCount();
    }
    catch(BaseException $e){ 
      throw new PDOException("Error Delete".$e->getMessage(). '<br/>');
    }

 
 }
 
 public static function getDocumentRestant($ad){
  $query="select document.ref,title,category.name as 'category',type.name as 'type',return_date 
          from document, adherent,loan,type,category 
          where document.id=loan.id_document 
          and loan.id_adherent=adherent.id 
          and document.id_type= type.id 
          and document.id_category=category.id 
          and loan.id_adherent=".$ad."";

   $pdo=Connexion::getConnection();
   
  $dbres=$pdo->query($query);
  $c=$dbres->fetchall(PDO::FETCH_OBJ);
 if(isset($c))     
    return $c;
  else
    return false;


 }

  
}
