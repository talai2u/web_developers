-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 08 Novembre 2014 à 09:39
-- Version du serveur :  5.6.20
-- Version de PHP :  5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `Mediatheque`
--

-- --------------------------------------------------------

--
-- Structure de la table `adherent`
--

CREATE TABLE IF NOT EXISTS `adherent` (
`id` int(11) NOT NULL,
  `ref` varchar(100) COLLATE utf8_bin NOT NULL,
  `lastname` varchar(224) COLLATE utf8_bin NOT NULL,
  `firstname` varchar(224) COLLATE utf8_bin NOT NULL,
  `phone` varchar(100) COLLATE utf8_bin NOT NULL,
  `adress` varchar(224) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `dateAdhesion` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=11 ;

--
-- Contenu de la table `adherent`
--

INSERT INTO `adherent` (`id`, `ref`, `lastname`, `firstname`, `phone`, `adress`, `email`, `dateAdhesion`) VALUES
(1, 'a1', 'Adherent1', 'Prenom1', '06 76 81 36 95', 'Rue oiseau N 10', 'Nom1@mail.com', '0000-00-00'),
(2, 'a2', 'Nom2', 'Prenom2', '06 76 81 36 95', 'Rue oiseau N 10', 'Nom2@mail.com', '0000-00-00'),
(3, 'a3', 'Nom3', 'Prenom3', '06 76 81 36 95', 'Rue oiseau N 10', 'Nom3@mail.com', '0000-00-00'),
(9, 'a4', 'Nom4', 'Prenom4', '06 76 81 36 95', 'Rue oiseau N 10', 'Nom4@mail.com', '0000-00-00'),
(10, 'a5', 'Nom5', 'Prenom5', '06 76 81 36 95', 'Rue oiseau N 10', 'Nom5@mail.com', '0000-00-00');

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

--
-- Contenu de la table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Action'),
(2, 'Aventure'),
(3, 'Comédie'),
(4, 'Classique'),
(5, 'Policier'),
(6, 'Romance'),
(7, 'Science Fiction');

-- --------------------------------------------------------

--
-- Structure de la table `connexion`
--

CREATE TABLE IF NOT EXISTS `connexion` (
`id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_bin NOT NULL,
  `pwd` varchar(100) COLLATE utf8_bin NOT NULL,
  `id_adherent` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Contenu de la table `connexion`
--

INSERT INTO `connexion` (`id`, `username`, `pwd`, `id_adherent`) VALUES
(1, 'user1', 'user1', 1),
(2, 'user2', 'user2', 2);

-- --------------------------------------------------------

--
-- Structure de la table `document`
--

CREATE TABLE IF NOT EXISTS `document` (
`id` int(11) NOT NULL,
  `ref` varchar(100) COLLATE utf8_bin NOT NULL,
  `title` varchar(100) COLLATE utf8_bin NOT NULL,
  `description` varchar(244) COLLATE utf8_bin NOT NULL,
  `edition` varchar(100) COLLATE utf8_bin NOT NULL,
  `id_state` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `image` varchar(254) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=12 ;

--
-- Contenu de la table `document`
--

INSERT INTO `document` (`id`, `ref`, `title`, `description`, `edition`, `id_state`, `id_category`, `id_type`, `image`) VALUES
(1, 'd1', 'Livre1', 'Moderatur partem quod agam accusationis patiebatur accusationis decebat et Vellem.', '2012', 1, 1, 1, ''),
(2, 'd2', 'Livre2', 'Moderatur partem quod agam accusationis patiebatur accusationis decebat et Vellem.', '2012', 1, 2, 1, ''),
(3, 'd3', 'CD1', 'Moderatur partem quod agam accusationis patiebatur accusationis decebat et Vellem.', '2013', 1, 3, 2, ''),
(9, 'd5', 'DVD1', 'Moderatur partem quod agam accusationis patiebatur accusationis decebat et Vellem.', '2014', 1, 1, 3, ''),
(10, 'd6', 'DVD2', 'Moderatur partem quod agam accusationis patiebatur accusationis decebat et Vellem.', '2015', 1, 4, 3, ''),
(11, 'kakdkkaldl', 'akdkka', 'kadmlal', '2014', 1, 5, 2, 'null');

-- --------------------------------------------------------

--
-- Structure de la table `loan`
--

CREATE TABLE IF NOT EXISTS `loan` (
`id` int(11) NOT NULL,
  `loaning_date` date NOT NULL,
  `return_date` date NOT NULL,
  `id_adherent` int(11) NOT NULL,
  `id_document` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=42 ;

--
-- Contenu de la table `loan`
--

INSERT INTO `loan` (`id`, `loaning_date`, `return_date`, `id_adherent`, `id_document`) VALUES
(39, '2014-11-06', '2014-11-19', 1, 3),
(40, '2014-11-06', '2014-11-19', 1, 9),
(41, '2014-11-06', '2014-11-19', 1, 10);

-- --------------------------------------------------------

--
-- Structure de la table `state`
--

CREATE TABLE IF NOT EXISTS `state` (
`id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Contenu de la table `state`
--

INSERT INTO `state` (`id`, `name`) VALUES
(1, 'disponible'),
(2, 'emprunté'),
(3, 'indisponible');

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

CREATE TABLE IF NOT EXISTS `type` (
`id` int(11) NOT NULL,
  `name` varchar(224) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Contenu de la table `type`
--

INSERT INTO `type` (`id`, `name`) VALUES
(1, 'Livre'),
(2, 'CD'),
(3, 'DVD');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `adherent`
--
ALTER TABLE `adherent`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `ref` (`ref`);

--
-- Index pour la table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `connexion`
--
ALTER TABLE `connexion`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`,`id_adherent`), ADD UNIQUE KEY `username_2` (`username`,`id_adherent`), ADD KEY `id_adherent` (`id_adherent`);

--
-- Index pour la table `document`
--
ALTER TABLE `document`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `ref` (`ref`), ADD KEY `id_state` (`id_state`), ADD KEY `id_category` (`id_category`), ADD KEY `id_type` (`id_type`);

--
-- Index pour la table `loan`
--
ALTER TABLE `loan`
 ADD PRIMARY KEY (`id`), ADD KEY `id_adherent` (`id_adherent`), ADD KEY `id_document` (`id_document`);

--
-- Index pour la table `state`
--
ALTER TABLE `state`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `type`
--
ALTER TABLE `type`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `adherent`
--
ALTER TABLE `adherent`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `connexion`
--
ALTER TABLE `connexion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `document`
--
ALTER TABLE `document`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `loan`
--
ALTER TABLE `loan`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT pour la table `state`
--
ALTER TABLE `state`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `type`
--
ALTER TABLE `type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `connexion`
--
ALTER TABLE `connexion`
ADD CONSTRAINT `connexion_ibfk_1` FOREIGN KEY (`id_adherent`) REFERENCES `adherent` (`id`);

--
-- Contraintes pour la table `document`
--
ALTER TABLE `document`
ADD CONSTRAINT `document_ibfk_1` FOREIGN KEY (`id_state`) REFERENCES `state` (`id`),
ADD CONSTRAINT `document_ibfk_2` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`),
ADD CONSTRAINT `document_ibfk_3` FOREIGN KEY (`id_type`) REFERENCES `type` (`id`);

--
-- Contraintes pour la table `loan`
--
ALTER TABLE `loan`
ADD CONSTRAINT `loan_ibfk_1` FOREIGN KEY (`id_adherent`) REFERENCES `adherent` (`id`),
ADD CONSTRAINT `loan_ibfk_2` FOREIGN KEY (`id_document`) REFERENCES `document` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
